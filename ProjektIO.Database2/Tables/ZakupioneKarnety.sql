﻿CREATE TABLE [dbo].[ZakupioneKarnety]
(
	[Id] BIGINT NOT NULL  IDENTITY(1,1), 
    [KlientId] BIGINT NOT NULL , 
    [KarnetId] BIGINT NOT NULL , 
    [DataWygasniecia] DATETIME2 NOT NULL, 
    CONSTRAINT [PK_ZakupioneKarnety] PRIMARY KEY ([Id]), 
    CONSTRAINT [FK_ZakupioneKarnety_Klienci] FOREIGN KEY ([KlientId]) REFERENCES [Klienci]([Id]), 
    CONSTRAINT [FK_ZakupioneKarnety_Karnety] FOREIGN KEY ([KarnetId]) REFERENCES [Karnety]([Id])
)
