﻿CREATE TABLE [dbo].[Stadiony]
(
	[Id] BIGINT NOT NULL  IDENTITY(1,1),  
    [Nazwa] NVARCHAR(50) NOT NULL, 
    CONSTRAINT [PK_Stadiony] PRIMARY KEY ([Id])
)
