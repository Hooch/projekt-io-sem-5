﻿CREATE TABLE [dbo].[ZakupioneBilety]
(
	[Id] BIGINT NOT NULL  IDENTITY(1,1), 
    [KlientId] BIGINT NOT NULL , 
    [ZakupionyKarnetId] BIGINT NULL , 
    [BiletId] BIGINT NOT NULL , 
    [JestZnizkowy] BIT NOT NULL DEFAULT 0, 
    [Data] DATETIME2 NOT NULL, 
    [ZakupionyKarnetemZaDarmo] BIT NOT NULL DEFAULT 0, 
    CONSTRAINT [PK_ZakupioneBilety] PRIMARY KEY ([Id]), 
    CONSTRAINT [FK_ZakupioneBilety_Klienci] FOREIGN KEY ([KlientId]) REFERENCES [Klienci]([Id]), 
    CONSTRAINT [FK_ZakupioneBilety_Bilety] FOREIGN KEY ([BiletId]) REFERENCES [Bilety]([Id]), 
    CONSTRAINT [FK_ZakupioneBilety_ZakupioneKarnety)] FOREIGN KEY ([ZakupionyKarnetId]) REFERENCES [ZakupioneKarnety]([Id]), 
)
