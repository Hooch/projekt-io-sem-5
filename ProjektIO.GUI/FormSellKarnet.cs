﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProjektIO.DatabaseContext;

namespace ProjektIO.GUI
{
	public partial class FormSellKarnet : Form
	{
		public FormSellKarnet()
		{
			InitializeComponent();

			using (var db = new IODatabase())
			{
				cbKarnety.DataSource = db.Karnety.ToList();
				if (cbKarnety.Items.Count > 0) cbKarnety.SelectedIndex = 0;

				cbKlienci.DataSource = db.Klienci.ToList();
				if (cbKlienci.Items.Count > 0) cbKlienci.SelectedIndex = 0;
			}
		}

		private void cbKlienci_Format(object sender, ListControlConvertEventArgs e)
		{
			string name = ((Klient)e.ListItem).Name;
			string surname = ((Klient)e.ListItem).Surname;

			e.Value = String.Format("{0} {1}", name, surname);
		}

		private void buttonSell1Month_Click(object sender, EventArgs e)
		{
			try
			{
				using (var db = new IODatabase())
				{
					var sprzedanyKarnet = new ZakupionyKarnet();
					sprzedanyKarnet.DataWygasniecia = DateTime.Now + TimeSpan.FromDays(31);
					sprzedanyKarnet.KarnetId = (long)cbKarnety.SelectedValue;
					sprzedanyKarnet.KlientId = (long)cbKlienci.SelectedValue;

					db.ZakupioneKarnety.Add(sprzedanyKarnet);
					db.SaveChanges();
				}

				DialogResult = DialogResult.OK;
				Close();
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
				DialogResult = DialogResult.Cancel;
				Close();
			}
		}
	}
}
