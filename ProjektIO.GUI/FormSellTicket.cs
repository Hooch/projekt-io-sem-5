﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProjektIO.DatabaseContext;

namespace ProjektIO.GUI
{
	public partial class FormSellTicket : Form
	{
		private IODatabase dbContext;

		public FormSellTicket()
		{
			dbContext = new IODatabase();
			InitializeComponent();

			cbStadion.DataSource = dbContext.Stadiony.ToList();
			if (cbStadion.Items.Count > 0) cbStadion.SelectedIndex = 0;

			cbKlienci.DataSource = dbContext.Klienci.ToList();
			if (cbKlienci.Items.Count > 0) cbKlienci.SelectedIndex = 0;
		}

		private void cbKlienci_Format(object sender, ListControlConvertEventArgs e)
		{
			string name = ((Klient)e.ListItem).Name;
			string surname = ((Klient)e.ListItem).Surname;

			e.Value = String.Format("{0} {1}", name, surname);
		}

		private void buttonSell_Click(object sender, EventArgs e)
		{
			try
			{
				Bilet bilet = (Bilet) cbBilety.SelectedItem;
				ZakupionyKarnet karnet = (ZakupionyKarnet) cbMozliweKarnety.SelectedItem;
				bool ulgowy = cbUlgowy.Checked;
				Klient klient = (Klient) cbKlienci.SelectedItem;
				bool karnetZaDarmo = rbDarmowy.Checked;
				bool karnetZnizka = rbZnizkowy.Checked;

				if (bilet == null || klient == null)
				{
					MessageBox.Show("Nie wybrano odpowiednich opcji.");
					return;
				}

				ZakupionyBilet zb = new ZakupionyBilet();
				zb.Bilet = bilet;
				zb.Data = DateTime.Now;
				zb.JestZnizkowy = ulgowy;
				zb.Klient = klient;

				if (!cbIgnorujKarnet.Checked)
				{
					zb.UzytyKarnet = karnet;
					if (karnetZaDarmo)
						zb.ZakupionyKarnetemZaDarmo = true;
					else if (karnetZnizka)
						zb.ZakupionyKarnetemZaDarmo = false;
				}

				dbContext.ZakupioneBilety.Add(zb);
				dbContext.SaveChanges();

				DialogResult = DialogResult.OK;
				Close();
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
				DialogResult = DialogResult.Cancel;
				Close();
			}
		}

		private void cbStadion_SelectedIndexChanged(object sender, EventArgs e)
		{
			cbMecz.DataSource = ((Stadion)cbStadion.SelectedItem).Mecze.ToList();
			if (cbMecz.Items.Count > 0) cbMecz.SelectedIndex = 0;
			cbMecz_SelectedIndexChanged(sender, e);

			cbMecz.Invalidate();
			cbBilety.Invalidate();
		}

		private void cbMecz_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cbMecz.SelectedItem == null)
			{
				return;
				cbBilety.DataSource = null;
			}

			cbBilety.DataSource = ((Mecz)cbMecz.SelectedItem).Bilety.ToList();
			if (cbBilety.Items.Count > 0)
			{
				cbBilety.SelectedIndex = 0;
			}
			cbBilety.Invalidate();
		}

		private void cbBilety_Format(object sender, ListControlConvertEventArgs e)
		{
			string sektor = ((Bilet)e.ListItem).Sektor;
			int miejsce = ((Bilet)e.ListItem).Miejsce;
			string vip = ((Bilet)e.ListItem).Vip ? " - VIP" : "";

			e.Value = String.Format("{0}: {1}{2}", sektor, miejsce, vip);
		}

		private void cbKlienci_SelectedIndexChanged(object sender, EventArgs e)
		{
			Klient klient = (Klient)cbKlienci.SelectedItem;
			Mecz mecz = (Mecz) cbMecz.SelectedItem;
			if (klient == null || mecz == null)
			{
				cbMozliweKarnety.DataSource = null;
				cbMozliweKarnety_SelectedIndexChanged(sender, e);
				return;
			}

			var aktualneKarnety = from zk in klient.ZakupioneKarnety
				where zk.DataWygasniecia >= mecz.Data
					&& ((zk.Karnet.IloscDostZnizekNaMecz > (
						from zb in zk.ZakupioneBilety
							where zb.Bilet.MeczId == mecz.Id
								&& !zb.ZakupionyKarnetemZaDarmo
							select zb).Count())

						||
						(from zb in zk.ZakupioneBilety
							where zb.ZakupionyKarnetemZaDarmo
							select zb).Count() < zk.Karnet.IloscDarmowychNaMecz
						)

				select zk;

			cbMozliweKarnety.DataSource = aktualneKarnety.ToList();

			cbMozliweKarnety_SelectedIndexChanged(sender, e);
			cbMozliweKarnety.Invalidate();
		}

		private void cbMozliweKarnety_Format(object sender, ListControlConvertEventArgs e)
		{
			e.Value = ((ZakupionyKarnet) e.ListItem).Karnet.Nazwa;
		}

		private void cbMozliweKarnety_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cbMozliweKarnety.SelectedValue == null)
			{
				rbDarmowy.Enabled = rbZnizkowy.Enabled = cbIgnorujKarnet.Enabled = false;
				cbIgnorujKarnet.Checked = true;
				rbDarmowy.Checked = rbZnizkowy.Checked = false;
				return;
			}


			Klient klient = (Klient)cbKlienci.SelectedItem;
			Mecz mecz = (Mecz) cbMecz.SelectedItem;
			ZakupionyKarnet zk = (ZakupionyKarnet) cbMozliweKarnety.SelectedItem;

			var iloscDarowychKupionych = (from zb in zk.ZakupioneBilety
											  where zb.Bilet.MeczId == mecz.Id
											  && zb.ZakupionyKarnetemZaDarmo
											  select zb).Count();

			var iloscZnizkowychKupionych = (from zb in zk.ZakupioneBilety
												where !zb.ZakupionyKarnetemZaDarmo
												&& zb.Bilet.MeczId == mecz.Id
												select zb).Count();

			rbDarmowy.Checked = false;
			rbZnizkowy.Checked = false;

			rbDarmowy.Enabled = iloscDarowychKupionych < zk.Karnet.IloscDarmowychNaMecz;
			rbZnizkowy.Enabled = iloscZnizkowychKupionych < zk.Karnet.IloscDostZnizekNaMecz;

			if (rbDarmowy.Enabled)
			{
				rbDarmowy.Checked = true;
			}
			else
			{
				rbZnizkowy.Checked = true;
			}

			if (rbDarmowy.Enabled || rbZnizkowy.Enabled)
			{
				cbIgnorujKarnet.Checked = false;
				cbIgnorujKarnet.Enabled = true;
			}
			else
			{
				cbIgnorujKarnet.Checked = true;
				cbIgnorujKarnet.Enabled = false;
			}
		}
	}
}
