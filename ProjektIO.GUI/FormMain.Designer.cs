﻿namespace ProjektIO.GUI
{
	partial class FormMain
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
			this.toolStripMain = new System.Windows.Forms.ToolStrip();
			this.toolStripButtonAddClient = new System.Windows.Forms.ToolStripButton();
			this.toolStripButtonSellKarnet = new System.Windows.Forms.ToolStripButton();
			this.toolStripButtonSellTicket = new System.Windows.Forms.ToolStripButton();
			this.toolStripMain.SuspendLayout();
			this.SuspendLayout();
			// 
			// toolStripMain
			// 
			this.toolStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonAddClient,
            this.toolStripButtonSellKarnet,
            this.toolStripButtonSellTicket});
			this.toolStripMain.Location = new System.Drawing.Point(0, 0);
			this.toolStripMain.Name = "toolStripMain";
			this.toolStripMain.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
			this.toolStripMain.Size = new System.Drawing.Size(618, 25);
			this.toolStripMain.TabIndex = 2;
			this.toolStripMain.Text = "Główny pasek";
			// 
			// toolStripButtonAddClient
			// 
			this.toolStripButtonAddClient.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.toolStripButtonAddClient.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonAddClient.Image")));
			this.toolStripButtonAddClient.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStripButtonAddClient.Name = "toolStripButtonAddClient";
			this.toolStripButtonAddClient.Size = new System.Drawing.Size(80, 22);
			this.toolStripButtonAddClient.Text = "Dodaj klienta";
			this.toolStripButtonAddClient.Click += new System.EventHandler(this.toolStripButtonAddClient_Click);
			// 
			// toolStripButtonSellKarnet
			// 
			this.toolStripButtonSellKarnet.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.toolStripButtonSellKarnet.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonSellKarnet.Image")));
			this.toolStripButtonSellKarnet.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStripButtonSellKarnet.Name = "toolStripButtonSellKarnet";
			this.toolStripButtonSellKarnet.Size = new System.Drawing.Size(91, 22);
			this.toolStripButtonSellKarnet.Text = "Sprzedaj karnet";
			this.toolStripButtonSellKarnet.Click += new System.EventHandler(this.toolStripButtonSellKarnet_Click);
			// 
			// toolStripButtonSellTicket
			// 
			this.toolStripButtonSellTicket.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.toolStripButtonSellTicket.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonSellTicket.Image")));
			this.toolStripButtonSellTicket.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStripButtonSellTicket.Name = "toolStripButtonSellTicket";
			this.toolStripButtonSellTicket.Size = new System.Drawing.Size(81, 22);
			this.toolStripButtonSellTicket.Text = "Sprzedaj Bilet";
			this.toolStripButtonSellTicket.Click += new System.EventHandler(this.toolStripButtonSellTicket_Click);
			// 
			// FormMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(618, 391);
			this.Controls.Add(this.toolStripMain);
			this.IsMdiContainer = true;
			this.Name = "FormMain";
			this.Text = "Projekt IO";
			this.toolStripMain.ResumeLayout(false);
			this.toolStripMain.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ToolStrip toolStripMain;
		private System.Windows.Forms.ToolStripButton toolStripButtonAddClient;
		private System.Windows.Forms.ToolStripButton toolStripButtonSellKarnet;
		private System.Windows.Forms.ToolStripButton toolStripButtonSellTicket;

	}
}

