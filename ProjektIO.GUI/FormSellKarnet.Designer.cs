﻿namespace ProjektIO.GUI
{
	partial class FormSellKarnet
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cbKarnety = new System.Windows.Forms.ComboBox();
			this.cbKlienci = new System.Windows.Forms.ComboBox();
			this.buttonSell1Month = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// cbKarnety
			// 
			this.cbKarnety.DisplayMember = "Nazwa";
			this.cbKarnety.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbKarnety.FormattingEnabled = true;
			this.cbKarnety.Location = new System.Drawing.Point(12, 12);
			this.cbKarnety.Name = "cbKarnety";
			this.cbKarnety.Size = new System.Drawing.Size(121, 21);
			this.cbKarnety.TabIndex = 0;
			this.cbKarnety.ValueMember = "Id";
			// 
			// cbKlienci
			// 
			this.cbKlienci.DisplayMember = "Id";
			this.cbKlienci.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbKlienci.FormattingEnabled = true;
			this.cbKlienci.Location = new System.Drawing.Point(139, 12);
			this.cbKlienci.MinimumSize = new System.Drawing.Size(250, 0);
			this.cbKlienci.Name = "cbKlienci";
			this.cbKlienci.Size = new System.Drawing.Size(250, 21);
			this.cbKlienci.TabIndex = 1;
			this.cbKlienci.ValueMember = "Id";
			this.cbKlienci.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.cbKlienci_Format);
			// 
			// buttonSell1Month
			// 
			this.buttonSell1Month.Location = new System.Drawing.Point(290, 39);
			this.buttonSell1Month.Name = "buttonSell1Month";
			this.buttonSell1Month.Size = new System.Drawing.Size(100, 23);
			this.buttonSell1Month.TabIndex = 2;
			this.buttonSell1Month.Text = "Sprzedaj 1 mc";
			this.buttonSell1Month.UseVisualStyleBackColor = true;
			this.buttonSell1Month.Click += new System.EventHandler(this.buttonSell1Month_Click);
			// 
			// FormSellKarnet
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(402, 77);
			this.Controls.Add(this.buttonSell1Month);
			this.Controls.Add(this.cbKlienci);
			this.Controls.Add(this.cbKarnety);
			this.Name = "FormSellKarnet";
			this.Text = "Sprzedaj karnet";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ComboBox cbKarnety;
		private System.Windows.Forms.ComboBox cbKlienci;
		private System.Windows.Forms.Button buttonSell1Month;
	}
}