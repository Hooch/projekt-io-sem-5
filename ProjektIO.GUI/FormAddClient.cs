﻿using System;
using System.Windows.Forms;
using ProjektIO.DatabaseContext;

namespace ProjektIO.GUI
{
	public partial class FormAddClient : Form
	{
		public FormAddClient()
		{
			InitializeComponent();
		}

		private void buttonAccept_Click(object sender, EventArgs e)
		{
			using (var dbCtx = new DatabaseContext.IODatabase())
			{
				dbCtx.Klienci.Add(new Klient()
				{
					Name = textBoxName.Text,
					Surname = textBoxSurname.Text
				});

				dbCtx.SaveChanges();
			}

			DialogResult = DialogResult.OK;
		}
	}
}
