﻿namespace ProjektIO.GUI
{
	partial class FormSellTicket
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cbStadion = new System.Windows.Forms.ComboBox();
			this.cbKlienci = new System.Windows.Forms.ComboBox();
			this.buttonSell = new System.Windows.Forms.Button();
			this.cbMecz = new System.Windows.Forms.ComboBox();
			this.cbBilety = new System.Windows.Forms.ComboBox();
			this.cbUlgowy = new System.Windows.Forms.CheckBox();
			this.cbMozliweKarnety = new System.Windows.Forms.ComboBox();
			this.rbDarmowy = new System.Windows.Forms.RadioButton();
			this.rbZnizkowy = new System.Windows.Forms.RadioButton();
			this.cbIgnorujKarnet = new System.Windows.Forms.CheckBox();
			this.SuspendLayout();
			// 
			// cbStadion
			// 
			this.cbStadion.DisplayMember = "Nazwa";
			this.cbStadion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbStadion.FormattingEnabled = true;
			this.cbStadion.Location = new System.Drawing.Point(12, 12);
			this.cbStadion.Name = "cbStadion";
			this.cbStadion.Size = new System.Drawing.Size(254, 21);
			this.cbStadion.TabIndex = 0;
			this.cbStadion.ValueMember = "Id";
			this.cbStadion.SelectedIndexChanged += new System.EventHandler(this.cbStadion_SelectedIndexChanged);
			// 
			// cbKlienci
			// 
			this.cbKlienci.DisplayMember = "Id";
			this.cbKlienci.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbKlienci.FormattingEnabled = true;
			this.cbKlienci.Location = new System.Drawing.Point(272, 12);
			this.cbKlienci.MinimumSize = new System.Drawing.Size(250, 0);
			this.cbKlienci.Name = "cbKlienci";
			this.cbKlienci.Size = new System.Drawing.Size(250, 21);
			this.cbKlienci.TabIndex = 1;
			this.cbKlienci.ValueMember = "Id";
			this.cbKlienci.SelectedIndexChanged += new System.EventHandler(this.cbKlienci_SelectedIndexChanged);
			this.cbKlienci.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.cbKlienci_Format);
			// 
			// buttonSell
			// 
			this.buttonSell.Location = new System.Drawing.Point(423, 66);
			this.buttonSell.Name = "buttonSell";
			this.buttonSell.Size = new System.Drawing.Size(99, 23);
			this.buttonSell.TabIndex = 2;
			this.buttonSell.Text = "Sprzedaj";
			this.buttonSell.UseVisualStyleBackColor = true;
			this.buttonSell.Click += new System.EventHandler(this.buttonSell_Click);
			// 
			// cbMecz
			// 
			this.cbMecz.DisplayMember = "Nazwa";
			this.cbMecz.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbMecz.FormattingEnabled = true;
			this.cbMecz.Location = new System.Drawing.Point(12, 39);
			this.cbMecz.Name = "cbMecz";
			this.cbMecz.Size = new System.Drawing.Size(254, 21);
			this.cbMecz.TabIndex = 3;
			this.cbMecz.ValueMember = "Id";
			this.cbMecz.SelectedIndexChanged += new System.EventHandler(this.cbMecz_SelectedIndexChanged);
			// 
			// cbBilety
			// 
			this.cbBilety.DisplayMember = "Id";
			this.cbBilety.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbBilety.FormattingEnabled = true;
			this.cbBilety.Location = new System.Drawing.Point(12, 66);
			this.cbBilety.Name = "cbBilety";
			this.cbBilety.Size = new System.Drawing.Size(254, 21);
			this.cbBilety.TabIndex = 4;
			this.cbBilety.ValueMember = "Id";
			this.cbBilety.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.cbBilety_Format);
			// 
			// cbUlgowy
			// 
			this.cbUlgowy.AutoSize = true;
			this.cbUlgowy.Location = new System.Drawing.Point(12, 93);
			this.cbUlgowy.Name = "cbUlgowy";
			this.cbUlgowy.Size = new System.Drawing.Size(61, 17);
			this.cbUlgowy.TabIndex = 5;
			this.cbUlgowy.Text = "Ulgowy";
			this.cbUlgowy.UseVisualStyleBackColor = true;
			// 
			// cbMozliweKarnety
			// 
			this.cbMozliweKarnety.DisplayMember = "Id";
			this.cbMozliweKarnety.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbMozliweKarnety.FormattingEnabled = true;
			this.cbMozliweKarnety.Location = new System.Drawing.Point(272, 39);
			this.cbMozliweKarnety.MinimumSize = new System.Drawing.Size(250, 0);
			this.cbMozliweKarnety.Name = "cbMozliweKarnety";
			this.cbMozliweKarnety.Size = new System.Drawing.Size(250, 21);
			this.cbMozliweKarnety.TabIndex = 6;
			this.cbMozliweKarnety.ValueMember = "Id";
			this.cbMozliweKarnety.SelectedIndexChanged += new System.EventHandler(this.cbMozliweKarnety_SelectedIndexChanged);
			this.cbMozliweKarnety.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.cbMozliweKarnety_Format);
			// 
			// rbDarmowy
			// 
			this.rbDarmowy.AutoSize = true;
			this.rbDarmowy.Location = new System.Drawing.Point(272, 66);
			this.rbDarmowy.Name = "rbDarmowy";
			this.rbDarmowy.Size = new System.Drawing.Size(69, 17);
			this.rbDarmowy.TabIndex = 7;
			this.rbDarmowy.TabStop = true;
			this.rbDarmowy.Text = "Darmowy";
			this.rbDarmowy.UseVisualStyleBackColor = true;
			// 
			// rbZnizkowy
			// 
			this.rbZnizkowy.AutoSize = true;
			this.rbZnizkowy.Location = new System.Drawing.Point(347, 66);
			this.rbZnizkowy.Name = "rbZnizkowy";
			this.rbZnizkowy.Size = new System.Drawing.Size(70, 17);
			this.rbZnizkowy.TabIndex = 8;
			this.rbZnizkowy.TabStop = true;
			this.rbZnizkowy.Text = "Znizkowy";
			this.rbZnizkowy.UseVisualStyleBackColor = true;
			// 
			// cbIgnorujKarnet
			// 
			this.cbIgnorujKarnet.AutoSize = true;
			this.cbIgnorujKarnet.Location = new System.Drawing.Point(272, 93);
			this.cbIgnorujKarnet.Name = "cbIgnorujKarnet";
			this.cbIgnorujKarnet.Size = new System.Drawing.Size(91, 17);
			this.cbIgnorujKarnet.TabIndex = 9;
			this.cbIgnorujKarnet.Text = "Ignoruj karnet";
			this.cbIgnorujKarnet.UseVisualStyleBackColor = true;
			// 
			// FormSellTicket
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(544, 122);
			this.Controls.Add(this.cbIgnorujKarnet);
			this.Controls.Add(this.rbZnizkowy);
			this.Controls.Add(this.rbDarmowy);
			this.Controls.Add(this.cbMozliweKarnety);
			this.Controls.Add(this.cbUlgowy);
			this.Controls.Add(this.cbBilety);
			this.Controls.Add(this.cbMecz);
			this.Controls.Add(this.buttonSell);
			this.Controls.Add(this.cbKlienci);
			this.Controls.Add(this.cbStadion);
			this.Name = "FormSellTicket";
			this.Text = "Sprzedaj bilet";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ComboBox cbStadion;
		private System.Windows.Forms.ComboBox cbKlienci;
		private System.Windows.Forms.Button buttonSell;
		private System.Windows.Forms.ComboBox cbMecz;
		private System.Windows.Forms.ComboBox cbBilety;
		private System.Windows.Forms.CheckBox cbUlgowy;
		private System.Windows.Forms.ComboBox cbMozliweKarnety;
		private System.Windows.Forms.RadioButton rbDarmowy;
		private System.Windows.Forms.RadioButton rbZnizkowy;
		private System.Windows.Forms.CheckBox cbIgnorujKarnet;
	}
}